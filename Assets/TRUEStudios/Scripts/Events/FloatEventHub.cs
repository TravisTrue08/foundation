﻿/******************************************************************************
 * Foundation Framework
 * Created by: Travis J True, 2016
 * This framework is free to use with no limitations.
******************************************************************************/

using UnityEngine;

namespace TRUEStudios.Events {
	[CreateAssetMenu(menuName = "TRUEStudios/Events/Hub (float)", fileName = "New Event Hub (float)")]
	public class FloatEventHub : EventHub<float> {}
}
