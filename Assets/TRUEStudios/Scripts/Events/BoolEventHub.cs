﻿/******************************************************************************
 * Foundation Framework
 * Created by: Travis J True, 2016
 * This framework is free to use with no limitations.
******************************************************************************/

using UnityEngine;

namespace TRUEStudios.Events {
	[CreateAssetMenu(menuName = "TRUEStudios/Events/Hub (bool)", fileName = "New Event Hub (bool)")]
	public class BoolEventHub : EventHub<bool> {}
}
